// Copyright (c) 2017 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

#![no_std]

#[macro_use]
extern crate serde_derive;
extern crate serde;
#[macro_use]
extern crate bitflags;

mod std {
    pub use core::*;
    pub mod string {
        pub struct String;
        impl String {
            pub fn from_utf8_lossy(_value: &[u8]) -> &'static str {
                ""
            }
        }
    }
}

bitflags! {
    #[derive(Serialize, Deserialize)]
    pub flags VMRights: u8 {
        const WRITE = 0b1,
        const READ = 0b10,
        const EXEC = 0b100,
    }
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum DataRegionRequest {
    GetSize,
    GetPermissions,
    SetPermissions { rights: VMRights }
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum DataRegionResponse {
    GetSize { size: u64 },
    GetPermissions { rights: VMRights },
    SetPermissions { new_rights: VMRights },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum AddressSpaceRequest {
    MapRegion { region: u8, rights: VMRights, vaddr: Option<u64> },
    UnmapRegion { region: u8 },
    RemapRegion { region: u8, new_rights: VMRights, new_vaddr: Option<u64> },
    VerifyRegion { region: u8 },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum AddressSpaceError {
    AlreadyMapped { start_addr: u64, len: u64 },
    NotARegion,
    NotMapped,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum AddressSpaceResponse {
    MapRegion { vaddr: u64 },
    UnmapRegion,
    RemapRegion { vaddr: u64 },
    VerifyRegion { is_valid: bool },
    Error(AddressSpaceError),
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum CapRegionRequest {
    GetSize,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum CapRegionResponse {
    GetSize { size: u64 },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum CapSpaceRequest {
    MapRegion { region: u8, cptr: Option<u64> },
    UnmapRegion { region: u8 },
    RemapRegion { region: u8, new_cptr: Option<u64> }
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum CapSpaceError {
    AlreadyMapped { start_cptr: u64, len: u64 },
    NotMapped,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum CapSpaceResponse {
    MapRegion { cptr: u64 },
    UnmapRegion,
    RemapRegion { cptr: u64 },
    Error(CapSpaceError),
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum SpaceBankRequest {
    RemainingQuota,
    EffectiveQuota,
    Destroy,
    CreateChild { quota: u64 },
    Verify { cap: u64 },
    BuyObject { object: FirmamentObjectKind },
    SellObject { cap: u64 },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum SpaceBankError {
    UnrecognizedObject,
    NotEnoughSpace,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum SpaceBankResponse {
    RemainingQuota { size: u64 },
    EffectiveQuota { size: u64 },
    Destroy,
    CreateChild { cap: u64 },
    Verify { valid: bool },
    BuyObject { cap: u64 },
    SellObject,
    Error(SpaceBankError),
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum FirmamentObjectKind {
    Thread,
    AddressSpace,
    CapSpace,
    DataRegion { size: usize },
    CapRegion { size: usize },
    Notification,
    Endpoint,
    WaitTree,
}

bitflags! {
    #[derive(Serialize, Deserialize)]
    pub flags InterestFlags: u8 {
        const LEVEL_TRIGGERED = 0b1,
        const ONESHOT = 0b10,
    }
}

// FIXME: WaitTree needs some actual design.

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum WaitTreeRequest {
    Wait,
    RegisterInterest { ntfn: u64, token: u64, flags: InterestFlags },
    DeregisterInterest { token: u64 },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum WaitTreeResponse {
    Wait { token: u64 },
    RegisterInterest,
    DeregisterInterest,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum SupervisorRequest {
    FutexWait { addr: u64, cur_val: u64, timeout: u64 },
    FutexWake { addr: u64, how_many: u64 },
    FutexRequeue { addr: u64, wake: u64, cur_val: u64, new_futex: u64, requeue_addr: u64 },
    Yield,
    CurrentAddressSpace,
    BoundSpaceBank,
    NewRecvCptr,
    BadgeCap { ep: u8, new_badge: u64 },
    CreateServer,
    EstablishConnection { server_handle: u8, can_block: bool },
    Listen { server_handle: u8, ep_badge: u64, can_block: bool },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub struct Connection {
    pub send_ntfn: u64,
    pub recv_ntfn: u64,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum SupervisorResponse {
    FutexWait(Option<FutexWaitResponse>),
    FutexWake,
    FutexRequeue(Option<FutexRequeueResponse>),
    Yield,
    CurrentAddressSpace { cptr: u64 },
    BoundSpaceBank { cptr: u64 },
    NewRecvCptr { cptr: u64 },
    BadgeCap { cptr: u64 },
    CreateServer { raw_ep: u64, readiness: u64, },
    EstablishConnection {
        was_ready: bool,
        connection: Option<Connection>,
    },
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum FutexWaitResponse {
    InvalidFutexPointer,
    BadState,
    TimeOut,
}

#[derive(Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum FutexRequeueResponse {
    InvalidFutexPointer,
    InvalidRequeuePointer,
    BadState,
}
